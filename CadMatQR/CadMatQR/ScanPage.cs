﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace CadMatQR
{
    public class ScanPage
    {
        public RelativeLayout CreateLayout(ZXingScannerPage scanPage)
        {
            var customOverlay = new RelativeLayout();

            var boxLeftTop = new BoxView
            {
                BackgroundColor = Color.Black,
            };
            var boxLeftBottom = new BoxView
            {
                BackgroundColor = Color.Black,
            };
            var boxRightTop = new BoxView
            {
                BackgroundColor = Color.Black,
            };
            var boxRightBottom = new BoxView
            {
                BackgroundColor = Color.Black,
            };
            //horizontalBox
            var boxLeftTopHorizontal = new BoxView
            {
                BackgroundColor = Color.Black,
            };
            var boxLeftBottomHorizontal = new BoxView
            {
                BackgroundColor = Color.Black,
            };
            var boxRightTopHorizontal = new BoxView
            {
                BackgroundColor = Color.Black,
            };
            var boxRightBottomHorizontal = new BoxView
            {
                BackgroundColor = Color.Black,
            };

            var torch = new Button
            {
                BackgroundColor = Color.White,
                Opacity = 0.8
            };

            torch.Clicked += delegate {
                scanPage.ToggleTorch();
            };

            //verticalBox
            customOverlay.Children.Add(boxLeftTop,
                widthConstraint: Constraint.RelativeToParent(Parent => QrBoxData.height),
                heightConstraint: Constraint.RelativeToParent(Parent => QrBoxData.width),
                xConstraint: Constraint.RelativeToParent(Parent => Parent.X + (Parent.Width * QrBoxData.factorLeft)),
                yConstraint: Constraint.RelativeToParent(Parent => Parent.Y + (Parent.Height * QrBoxData.factorTop)));

            customOverlay.Children.Add(boxLeftBottom,
                widthConstraint: Constraint.RelativeToParent(Parent => QrBoxData.height),
                heightConstraint: Constraint.RelativeToParent(Parent => QrBoxData.width),
                xConstraint: Constraint.RelativeToParent(Parent => Parent.X + (Parent.Width * QrBoxData.factorLeft)),
                yConstraint: Constraint.RelativeToParent(Parent => Parent.Y + (Parent.Height * QrBoxData.factorBottom)));

            customOverlay.Children.Add(boxRightTop,
                widthConstraint: Constraint.RelativeToParent(Parent => QrBoxData.height),
                heightConstraint: Constraint.RelativeToParent(Parent => QrBoxData.width),
                xConstraint: Constraint.RelativeToParent(Parent => Parent.X + (Parent.Width * QrBoxData.factorRight) + QrBoxData.width - QrBoxData.height),
                yConstraint: Constraint.RelativeToParent(Parent => Parent.Y + (Parent.Height * QrBoxData.factorTop)));

            customOverlay.Children.Add(boxRightBottom,
                widthConstraint: Constraint.RelativeToParent(Parent => QrBoxData.height),
                heightConstraint: Constraint.RelativeToParent(Parent => QrBoxData.width),
                xConstraint: Constraint.RelativeToParent(Parent => Parent.X + (Parent.Width * QrBoxData.factorRight) + QrBoxData.width - QrBoxData.height),
                yConstraint: Constraint.RelativeToParent(Parent => Parent.Y + (Parent.Height * QrBoxData.factorBottom)));

            //horizontalbox
            customOverlay.Children.Add(boxLeftTopHorizontal,
                widthConstraint: Constraint.RelativeToParent(Parent => QrBoxData.width),
                heightConstraint: Constraint.RelativeToParent(Parent => QrBoxData.height),
                xConstraint: Constraint.RelativeToParent(Parent => Parent.X + (Parent.Width * QrBoxData.factorLeft)),
                yConstraint: Constraint.RelativeToParent(Parent => Parent.Y + (Parent.Height * QrBoxData.factorTop)));

            customOverlay.Children.Add(boxLeftBottomHorizontal,
                widthConstraint: Constraint.RelativeToParent(Parent => QrBoxData.width),
                heightConstraint: Constraint.RelativeToParent(Parent => QrBoxData.height),
                xConstraint: Constraint.RelativeToParent(Parent => Parent.X + (Parent.Width * QrBoxData.factorLeft)),
                yConstraint: Constraint.RelativeToParent(Parent => Parent.Y + (Parent.Height * QrBoxData.factorBottom) + QrBoxData.width - QrBoxData.height));

            customOverlay.Children.Add(boxRightTopHorizontal,
                widthConstraint: Constraint.RelativeToParent(Parent => QrBoxData.width),
                heightConstraint: Constraint.RelativeToParent(Parent => QrBoxData.height),
                xConstraint: Constraint.RelativeToParent(Parent => Parent.X + (Parent.Width * QrBoxData.factorRight)),
                yConstraint: Constraint.RelativeToParent(Parent => Parent.Y + (Parent.Height * QrBoxData.factorTop)));

            customOverlay.Children.Add(boxRightBottomHorizontal,
                widthConstraint: Constraint.RelativeToParent(Parent => QrBoxData.width),
                heightConstraint: Constraint.RelativeToParent(Parent => QrBoxData.height),
                xConstraint: Constraint.RelativeToParent(Parent => Parent.X + (Parent.Width * QrBoxData.factorRight)),
                yConstraint: Constraint.RelativeToParent(Parent => Parent.Y + (Parent.Height * QrBoxData.factorBottom) + QrBoxData.width - QrBoxData.height));
            return customOverlay;
        }
    }
}
