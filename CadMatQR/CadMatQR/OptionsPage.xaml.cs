﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace CadMatQR
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OptionsPage : ContentPage
	{
        ZXingScannerPage scanPage;
        public OptionsPage ()
		{
			InitializeComponent ();
            ID.Text = Data.ID;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                var customOverlay = new ScanPage().CreateLayout(scanPage);
                scanPage = new ZXingScannerPage(new ZXing.Mobile.MobileBarcodeScanningOptions { AutoRotate = false, TryHarder = true }, customOverlay: customOverlay); scanPage.OnScanResult += (result) => {
                    scanPage.IsScanning = false;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Data.ID = result.Text;
                        App.Current.MainPage = new OptionsPage();
                    });
                };
                await Navigation.PushModalAsync(scanPage);
            }
            catch (Exception ex)
            {
                DisplayAlert("Status", "http://" + Data.ConnectionString + "/api/qrid/" + ID.Text, "Ok", ex.Message);
            }
        }
    }
}