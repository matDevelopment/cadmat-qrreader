﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CadMatQR
{
    static class SendData
    {
        public static void Get(string id)
        {
            var address = "http://" + Data.ConnectionString + "/api/qrid/" + id ;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse());
        }
    }
}
