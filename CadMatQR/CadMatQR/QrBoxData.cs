﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CadMatQR
{
    public static class QrBoxData
    {
        public const double factorLeft = 0.3;
        public const double factorTop = 0.3;
        public const double factorRight = 0.6;
        public const double factorBottom = 0.5;
        public const double width = 30;
        public const double height = 5;
    }
}
