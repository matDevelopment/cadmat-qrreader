	/* Data SHA1: 7eaacb321c2efe9c737a7bf31f4868531e0c96b3 */
	.arch	armv8-a
	.file	"typemap.mj.inc"

	/* Mapping header */
	.section	.data.mj_typemap,"aw",@progbits
	.type	mj_typemap_header, @object
	.p2align	2
	.global	mj_typemap_header
mj_typemap_header:
	/* version */
	.word	1
	/* entry-count */
	.word	575
	/* entry-length */
	.word	219
	/* value-offset */
	.word	123
	.size	mj_typemap_header, 16

	/* Mapping data */
	.type	mj_typemap, @object
	.global	mj_typemap
mj_typemap:
	.size	mj_typemap, 125926
	.include	"typemap.mj.inc"
