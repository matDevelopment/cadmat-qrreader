﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace CadMatQR
{
	public partial class MainPage : ContentPage
	{
        ZXingScannerPage scanPage;
		public MainPage()
		{
			InitializeComponent();
            if (Application.Current.Properties.ContainsKey("ConnectionString"))
            {
                Entry.Text = Application.Current.Properties["ConnectionString"].ToString();
                Application.Current.SavePropertiesAsync();
            }
            Data.ConnectionString = Entry.Text;
        }

        private async void ButtonScanCustom_Clicked(object sender, EventArgs e)
        {

            var customOverlay = new ScanPage().CreateLayout(scanPage);
            scanPage = new ZXingScannerPage(new ZXing.Mobile.MobileBarcodeScanningOptions { AutoRotate = false, TryHarder = true }, customOverlay: customOverlay); scanPage.OnScanResult += (result) => {
                scanPage.IsScanning = false;
                
                Device.BeginInvokeOnMainThread( () =>
                {
                    Data.ID = result.Text;
                    App.Current.MainPage = new OptionsPage();
                });
            };
            await Navigation.PushModalAsync(scanPage);
        }

        private void Entry_Completed(object sender, EventArgs e)
        {
            Application.Current.Properties["ConnectionString"] = Entry.Text;
            Application.Current.SavePropertiesAsync();
            Data.ConnectionString = Entry.Text;
        }
    }
}
